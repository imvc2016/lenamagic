#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include "opencv2\opencv.hpp"
#include <opencv2/opencv.hpp>//キー操作

using namespace cv; //トリミングのソースコードにあった

//キー操作
const int CV_WAITKEY_CURSORKEY_TOP = 2490368;
const int CV_WAITKEY_CURSORKEY_BOTTOM = 2621440;
const int CV_WAITKEY_CURSORKEY_RIGHT = 2555904;
const int CV_WAITKEY_CURSORKEY_LEFT = 2424832;

int main(int argc, const char* argv[])
{
	//namedWindow("ROI"); //デバッグ
	// 画像の読み込みに失敗したらエラー終了する
	cv::Mat src = cv::imread("lena2.jpg", cv::IMREAD_COLOR);
	cv::Mat dst = src.clone();
	if (src.empty())
	{
		std::cerr << "画像がありません." << std::endl;
		return -1;
	}

	//モザイク座標
	cv::Rect roi_rect(215, 180, 30, 30); // x,y,w,h
	cv::Rect roi_rect2(310, 225, 20, 20); // x,y,w,h
	cv::Mat src_roi = src(roi_rect);
	cv::Mat dst_roi = dst(roi_rect);
	cv::Mat src_roi2 = src(roi_rect2);
	cv::Mat dst_roi2 = dst(roi_rect2);

	// ぼかしの処理
	cv::blur(src_roi, dst_roi, cv::Size(200, 200));
	cv::blur(src_roi2, dst_roi2, cv::Size(200, 200));
	cv::Mat roi(dst, Rect(90, 0, 160, 160));//トリミング初期表示画像
	//imshow("ROI", dst); //レナさん全体表示：デバック

	int keycode = 0;
	int width = 10;
	int height = 10;
	imshow("レナマジック！！！！！！！！！！！！！！！！！", roi);
	
	while (true)  {
		switch (keycode){

			case CV_WAITKEY_CURSORKEY_TOP:
				std::cout << "↑" << std::endl; //コンソールに文字を表示
				roi = cv::Mat(dst, Rect(90, 0, 160 + width, 160 + height));	/*レナさんのトリミング */
				height -= 10;
				imshow("レナマジック！！！！！！！！！！！！！！！！！", roi);
				break;

			case CV_WAITKEY_CURSORKEY_LEFT:
				std::cout << "←" << std::endl;
				roi = cv::Mat(dst, Rect(90, 0, 160 + width, 160 + height));	/*レナさんのトリミング */
				width -= 10;
				imshow("レナマジック！！！！！！！！！！！！！！！！！", roi);
				break;

			case CV_WAITKEY_CURSORKEY_RIGHT:
				std::cout << "→" << std::endl;
				roi = cv::Mat(dst, Rect(90, 0, 160 + width, 160 + height ));	/*レナさんのトリミング */
				width += 10;
				imshow("レナマジック！！！！！！！！！！！！！！！！！", roi);
				break;

			case CV_WAITKEY_CURSORKEY_BOTTOM:
				std::cout << "↓" << std::endl;
				roi = cv::Mat(dst, Rect(90, 0, 160 + width, 160 + height));	/*レナさんのトリミング */
				height += 10;
				imshow("レナマジック！！！！！！！！！！！！！！！！！", roi);
				break;

			default:
				break;
						}	
			keycode = cv::waitKey(30);
				  }

	cv::waitKey(0);
	cv::destroyAllWindows();
	return 0;
}